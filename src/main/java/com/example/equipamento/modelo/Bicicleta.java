package com.example.equipamento.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Bicicleta {

    private int idBicicleta;

    private String marca;

    private String modelo;

    private String ano;

    private int numero;

    private String status;

}
