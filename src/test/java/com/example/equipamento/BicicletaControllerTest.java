package com.example.equipamento;

import org.junit.jupiter.api.Test;
import com.example.equipamento.controller.BicicletaController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
    @SpringBootTest
    public class BicicletaControllerTest {

        @MockBean
        private BicicletaController bicicletaController;

        @BeforeEach
        public void setup() {
            bicicletaController = new BicicletaController();
        }

        @Test
        @DisplayName("Deve retornar uma mensagem de olá quando o endpoint Ola for atingido")
        public void RetornaOla() {
            String esperada = "Olá Mundoooo";
            String actual = bicicletaController.Ola();
            assertEquals(esperada, actual);
        }
    }


